package services;

import dao.ThemeOfCourseDAO;
import dto.ThemeOfCourseDTO;
import models.ThemeOfCourse;

import java.util.ArrayList;
import java.util.List;

public class ThemeOfCourseService {

    private ThemeOfCourseDAO themeOfCourseDAO = new ThemeOfCourseDAO();

    public ThemeOfCourseDTO findThemesById(int id) {
        ThemeOfCourseDTO themeOfCourse = themeModelToDTO(themeOfCourseDAO.findById(id));
        if (themeOfCourse!=null){
            return themeOfCourse;
        }
        else{
            throw new NullPointerException();
        }
    }

    public void saveTheme(ThemeOfCourse themeOfCourse) {
        if(themeOfCourse.getScience() != null){
            themeOfCourseDAO.save(themeOfCourse);
        }else{
            throw new NullPointerException();
        }
    }

    public void deleteTheme(int id) {
        if (id>=0){
            themeOfCourseDAO.delete(id);
        }
        else{
            throw new NullPointerException();
        }
    }

    public void updateTheme(ThemeOfCourse themeOfCourse) {
        if (themeOfCourse!=null){
            themeOfCourseDAO.update(themeOfCourse);
        }
        else{
            throw new NullPointerException();
        }
    }

    public List<ThemeOfCourseDTO> findAllThemes(int page) {
        List<ThemeOfCourse> theme = themeOfCourseDAO.findAll(page);
        if (!theme.isEmpty()){
            return themeDTOList(theme);
        }
        else {
            throw new NullPointerException();
        }
    }

    public List<ThemeOfCourseDTO> findThemesByName(String name, int page) {
        List<ThemeOfCourseDTO> themeOfCourses = themeDTOList(themeOfCourseDAO.findByScience(name, page));
        if(themeOfCourses.isEmpty()){
            throw new NullPointerException();
        }
        else{
            return themeOfCourses;
        }
    }

    public ThemeOfCourseDTO themeModelToDTO(ThemeOfCourse themeOfCourse){
        ThemeOfCourseDTO dto = new ThemeOfCourseDTO(themeOfCourse.getId(), themeOfCourse.getScience());
        return dto;
    }

    public List<ThemeOfCourseDTO> themeDTOList(List<ThemeOfCourse> theme){
        List<ThemeOfCourseDTO> themeOfCourseDTOList = new ArrayList<>();
        ThemeOfCourseDTO themeOfCourseDTO;
        for (int i=0; i<theme.size(); i++){
            themeOfCourseDTO = new ThemeOfCourseDTO(theme.get(i).getId(), theme.get(i).getScience());
            themeOfCourseDTOList.add(themeOfCourseDTO);
        }
        return themeOfCourseDTOList;
    }
}
