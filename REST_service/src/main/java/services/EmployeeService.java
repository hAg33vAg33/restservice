package services;

import dao.DepartmentDAO;
import dao.EmployeeDAO;
import dao.PositionDAO;
import dto.*;
import models.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import java.util.Date;


public class EmployeeService {

    private EmployeeDAO employeeDAO = new EmployeeDAO();

    private DepartmentDAO departmentDAO = new DepartmentDAO();

    private PositionDAO positionDAO = new PositionDAO();

    public void saveEmployee(Employee employee) {
        Calendar calendar = new GregorianCalendar(2018, 1 , 1);
        Date startDate = calendar.getTime();
        Date endDate = new Date();
        if (employee.getAge() > 17 && (employee.getEmploymentDate().after(startDate) && employee.getEmploymentDate().before(endDate))
                && employee.getDepartment() != null && employee.getPosition() != null){
            employeeDAO.save(employee);
        }
        else{
            throw new NullPointerException();
        }
    }

    public void deleteEmployee(UUID specialNumber) {
        Employee employee = employeeDAO.findBySpecialNumber(specialNumber);
        if (employee!=null){
            employeeDAO.delete(employee);
        }
        else{
            throw new NullPointerException();
        }
    }

    public void updateEmployee(Employee employee) {
        Calendar calendar = new GregorianCalendar(2018, 1 , 1);
        Date startDate = calendar.getTime();
        Date endDate = new Date();
        if (employee.getAge() > 17 && (employee.getEmploymentDate().after(startDate) && employee.getEmploymentDate().before(endDate))
                && employee.getDepartment() != null && employee.getPosition() != null){
            employeeDAO.update(employee);
        }
        else{
            throw new NullPointerException();
        }
    }

    public List<EmployeeDTO> findAllEmployees(int page) {
        List<Employee> employees = employeeDAO.findAll(page);
        if (!employees.isEmpty()){
            return employeeDTOList(employees);
        }
        else {
            throw new NullPointerException();
        }
    }

    public List<EmployeeDTO> findByDepartmentAndPosition(int departmentId, int positionId, int page){
        DepartmentDTO department = departmentModelToDTO(departmentDAO.findById(departmentId));
        PositionDTO position = positionModelToDTO(positionDAO.findById(positionId));
        List<Employee> employee = employeeDAO.findByDepartmentAndPosition(departmentId, positionId, page);
        if (department != null && position != null && !employee.isEmpty()){
            return employeeDTOList(employee);
        }
        else{
            throw new NullPointerException();
        }

    }

    public List<EmployeeDTO> findByDepartment(int departmentId, int page){
        DepartmentDTO department = departmentModelToDTO(departmentDAO.findById(departmentId));
        List<EmployeeDTO> employee = employeeDTOList(employeeDAO.findByDepartment(departmentId, page));
        if (department != null && !employee.isEmpty()) return employee;
        else{
            throw new NullPointerException();
        }
    }

    public List<EmployeeDTO> findByPosition(int positionId, int page){
        PositionDTO position = positionModelToDTO(positionDAO.findById(positionId));
        List<EmployeeDTO> employee = employeeDTOList(employeeDAO.findByPosition(positionId, page));
        if (position != null && !employee.isEmpty()) return employee;
        else{
            throw new NullPointerException();
        }
    }

    public List<EmployeeDTO> findByEmploymentDateRange(String startDate, String endDate, int page) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = formatter.parse(startDate);
        Date date2 = formatter.parse(endDate);
        if(date1.before(date2) && date2 != null && date1 != null) {
            return employeeDTOList(employeeDAO.findByEmploymentDateRange(date1, date2, page));
        } else {
            throw new NullPointerException();
        }
    }

    public List<EmployeeDTO> findByEmploymentDate(String date, int page) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date searchDate = formatter.parse(date);
        Calendar calendar = new GregorianCalendar(2018, 1 , 1);
        Date startDate = calendar.getTime();
        Date endDate = new Date();
        if (searchDate.after(startDate) && searchDate.before(endDate) && date != null){

            List<Employee> employees = employeeDAO.findByEmploymentDate(searchDate, page);
            if (!employees.isEmpty()) {
                return employeeDTOList(employees);
            }else   {
                throw new NullPointerException();
            }
        } else {
            throw new NullPointerException();
        }
    }

    public List<EmployeeDTO> findByFirstName(String firstName, int page){
        List<EmployeeDTO> employee = employeeDTOList(employeeDAO.findByFirstName(firstName, page));
        if (employee.isEmpty()){
            throw new NullPointerException();
        } else {
            return employee;
        }
    }

    public List<EmployeeDTO> findByLastName(String lastName, int page){
        List<EmployeeDTO> employee = employeeDTOList(employeeDAO.findByLastName(lastName, page));
        if (employee.isEmpty()){
            throw new NullPointerException();
        } else {
            return employee;
        }
    }

    public List<EmployeeDTO> findByPassportData(String passportData, int page){
        List<EmployeeDTO> employee = employeeDTOList(employeeDAO.findByPassportData(passportData, page));
        if (employee.isEmpty()){
            throw new NullPointerException();
        }
        else {
            return employee;
        }
    }

    public List<EmployeeDTO> findByAge(int age, int page){
        List<EmployeeDTO> employee = employeeDTOList(employeeDAO.findByAge(age, page));
        if (employee.isEmpty()){
            throw new NullPointerException();
        }
        else {
            return employee;
        }
    }

    public List<EmployeeDTO> findByAgeRange(int startAge, int endAge, int page){
        List<EmployeeDTO> employee = employeeDTOList(employeeDAO.findByAgeRange(startAge, endAge, page));
        if (employee.isEmpty()){
            throw new NullPointerException();
        }
        else {
            return employee;
        }
    }

    public List<EmployeeDTO> findByAgeSorted(int page){
        List<EmployeeDTO> employee = employeeDTOList(employeeDAO.findByAgeSorted(page));
        if (employee.isEmpty()){
            throw new NullPointerException();
        }
        else {
            return employee;
        }
    }

    public List<EmployeeDTO> findByEmploymentDateSorted(int page){
        List<EmployeeDTO> employee = employeeDTOList(employeeDAO.findByEmploymentDateSorted(page));
        if (employee.isEmpty()){
            throw new NullPointerException();
        }
        else {
            return employee;
        }
    }

    public EmployeeDTO findByAgeMax(){
        EmployeeDTO employee = employeeModelToDTO(employeeDAO.findByAgeMax());
        if (employee==null){
            throw new NullPointerException();
        }
        else {
            return employee;
        }
    }

    public List<EmployeeDTO> extendedSearch(String search, int page){
        List<EmployeeDTO> employee = employeeDTOList(employeeDAO.extendedSearch(search, page));
        if (employee.isEmpty()){
            throw new NullPointerException();
        }
        else {
            return employee;
        }
    }

    public DepartmentDTO departmentModelToDTO(Department department){
        DepartmentDTO dto = new DepartmentDTO(department.getId(), department.getName());
        return dto;
    }

    public PositionDTO positionModelToDTO(Position position){
        PositionDTO dto = new PositionDTO(position.getId(), position.getName(), position.getSalary());
        return dto;
    }

    public EmployeeDTO employeeModelToDTO(Employee employee){
        EmployeeDTO dto = new EmployeeDTO(employee.getId(), employee.getLastName(), employee.getFirstName(),
                employee.getAge(), employee.getEmploymentDate(), employee.getPassportData(),
                departmentModelToDTO(employee.getDepartment()), positionModelToDTO(employee.getPosition()));
        return dto;
    }

    public List<DepartmentDTO> departmentDTOList(List<Department> departments){
        List<DepartmentDTO> departmentDTOList = new ArrayList<>();
        DepartmentDTO departmentDTO;
        for (int i=0; i<departments.size(); i++){
            departmentDTO = new DepartmentDTO(departments.get(i).getId(), departments.get(i).getName());
            departmentDTOList.add(departmentDTO);
        }
        return departmentDTOList;
    }

    public List<PositionDTO> positionDTOList(List<Position> positions){
        List<PositionDTO> positionDTOList = new ArrayList<>();
        PositionDTO positionDTO;
        for (int i=0; i<positions.size(); i++){
            positionDTO = new PositionDTO(positions.get(i).getId(), positions.get(i).getName(), positions.get(i).getSalary());
            positionDTOList.add(positionDTO);
        }
        return positionDTOList;
    }

    public List<EmployeeDTO> employeeDTOList(List<Employee> employees){
        List<EmployeeDTO> employeeDTOList = new ArrayList<>();
        EmployeeDTO employeeDTO;
        for (int i=0; i<employees.size(); i++){
            employeeDTO = new EmployeeDTO(employees.get(i).getId(), employees.get(i).getLastName(), employees.get(i).getFirstName(),
                    employees.get(i).getAge(), employees.get(i).getEmploymentDate(), employees.get(i).getPassportData(),
                    departmentModelToDTO(employees.get(i).getDepartment()), positionModelToDTO(employees.get(i).getPosition()));
            employeeDTOList.add(employeeDTO);
        }
        return employeeDTOList;
    }
}
