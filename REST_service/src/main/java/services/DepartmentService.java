package services;


import dao.DepartmentDAO;
import dto.DepartmentDTO;
import models.Department;

import java.util.ArrayList;
import java.util.List;

public class DepartmentService {

    private DepartmentDAO departmentDAO = new DepartmentDAO();

    public DepartmentDTO findDepartmentById(int id) {
        DepartmentDTO department = departmentModelToDTO(departmentDAO.findById(id));
        if (department!=null){
            return department;
        }
        else{
            throw new NullPointerException();
        }
    }

    public void saveDepartment(Department department) {
        if(department.getName()!=null){
            departmentDAO.save(department);
        }else {
            throw new NullPointerException();
        }

    }

    public void deleteDepartment(int id) {
        if (id>0){
            departmentDAO.delete(id);
        }
        else{
            throw new NullPointerException();
        }
    }

    public void updateDepartment(Department department) {
        if (department!=null){
            departmentDAO.update(department);
        }
        else{
            throw new NullPointerException();
        }
    }

    public List<DepartmentDTO> findAllDepartments(int page) {
        List<Department> departments = departmentDAO.findAll(page);
        if (!departments.isEmpty()){
            return departmentDTOList(departments);
        }
        else {
            throw new NullPointerException();
        }
    }

    public List<DepartmentDTO> findDepartmentByName(String name, int page) {
        List<DepartmentDTO> departments = departmentDTOList(departmentDAO.findByName(name, page));
        if(departments.isEmpty()){
            throw new NullPointerException();
        }
        else{
            return departments;
        }
    }

    public DepartmentDTO departmentModelToDTO(Department department){
        DepartmentDTO dto = new DepartmentDTO(department.getId(), department.getName());
        return dto;
    }

    public List<DepartmentDTO> departmentDTOList(List<Department> departments){
        List<DepartmentDTO> departmentDTOList = new ArrayList<>();
        DepartmentDTO departmentDTO;
        for (int i=0; i<departments.size(); i++){
            departmentDTO = new DepartmentDTO(departments.get(i).getId(), departments.get(i).getName());
            departmentDTOList.add(departmentDTO);
        }
        return departmentDTOList;
    }
}
