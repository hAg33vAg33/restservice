package services;

import dao.PositionDAO;
import dto.PositionDTO;
import models.Position;

import java.util.*;


public class PositionService {

    private PositionDAO positionDAO = new PositionDAO();

    public PositionDTO findPositionById(int id) {
        PositionDTO position = positionModelToDTO(positionDAO.findById(id));
        if (position!=null){
            return position;
        }
        else{
            throw new NullPointerException();
        }
    }

    public void savePosition(Position position) {
        if (position.getSalary() >1000 && position.getSalary()<10000){
            positionDAO.save(position);
        }else{
            throw new NullPointerException();
        }

    }

    public void deletePosition(int id) {
        if (id>0){
            positionDAO.delete(id);
        }
        else{
            throw new NullPointerException();
        }
    }

    public void updatePosition(Position position) {
        if (position!=null && position.getSalary() > 1000 && position.getSalary()<10000){
            positionDAO.update(position);
        }
        else{
            throw new NullPointerException();
        }
    }

    public List<PositionDTO> findAllPositions(int page) {
        List<PositionDTO> positions = positionDTOList(positionDAO.findAll(page));
        if(positions.isEmpty()){
            throw new NullPointerException();
        }
        else{
            return positions;
        }
    }

    public List<PositionDTO> findPositionsByName(String name, int page) {
        List<PositionDTO> positions = positionDTOList(positionDAO.findByName(name, page));
        if(positions.isEmpty()){
            throw new NullPointerException();
        }
        else{
            return positions;
        }
    }

    public List<PositionDTO> findBySalarySorted(int page) {
        List<Position> position = positionDAO.findBySalarySorted(page);
        if (!position.isEmpty()){
            return positionDTOList(position);
        }
        else {
            throw new NullPointerException();
        }
    }

    public PositionDTO positionModelToDTO(Position position){
        PositionDTO dto = new PositionDTO(position.getId(), position.getName(), position.getSalary());
        return dto;
    }

    public List<PositionDTO> positionDTOList(List<Position> positions){
        List<PositionDTO> positionDTOList = new ArrayList<>();
        PositionDTO positionDTO;
        for (int i=0; i<positions.size(); i++){
            positionDTO = new PositionDTO(positions.get(i).getId(), positions.get(i).getName(), positions.get(i).getSalary());
            positionDTOList.add(positionDTO);
        }
        return positionDTOList;
    }
}
