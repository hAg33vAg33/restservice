package services;
import dao.DepartmentDAO;
import dao.CourseDAO;
import dao.ThemeOfCourseDAO;
import dto.CourseDTO;
import dto.DepartmentDTO;
import dto.RevenueDTO;
import dto.ThemeOfCourseDTO;
import models.*;

import java.util.ArrayList;
import java.util.List;

public class CourseService {

    private CourseDAO courseDAO = new CourseDAO();

    private DepartmentDAO departmentDAO = new DepartmentDAO();

    private ThemeOfCourseDAO themeOfCourseDAO = new ThemeOfCourseDAO();

    public CourseDTO findCourseById(int id) {
        Course course = courseDAO.findById(id);
        DepartmentDTO departmentDTO = departmentModelToDTO(course.getDepartment());
        ThemeOfCourseDTO themeOfCourseDTO = themeModelToDTO(course.getThemeOfCourse());
        CourseDTO courseDTO = new CourseDTO(course.getId(), course.getName(),
                themeOfCourseDTO, course.getPrice(), course.getSoldCount(), departmentDTO);
        if (courseDTO!=null){
            return courseDTO;
        }
        else{
            throw new NullPointerException();
        }
    }

    public List<CourseDTO> findAll(int id) {
        List<CourseDTO> course = coursesDTOList(courseDAO.findAll(id));
        if (course!=null){
            return course;
        }
        else{
            throw new NullPointerException();
        }
    }

    public void saveCourse(Course course) {
        if (course.getDepartment()!=null && course.getThemeOfCourse()!=null && course!=null) {
            courseDAO.save(course);
        }else{
            throw new NullPointerException();
        }

    }

    public void deleteCourse(int id) {
        Course course = courseDAO.findById(id);
        if (course!=null){
            courseDAO.delete(course);
        }
        else{
            throw new NullPointerException();
        }
    }

    public void updateCourse(Course course) {
        if (course.getDepartment()!=null && course.getThemeOfCourse()!=null && course !=null){
            courseDAO.update(course);
        }else{
            throw new NullPointerException();
        }

    }

    public List<CourseDTO> findCourseByName(String name, int page) {
        List<CourseDTO> courses = coursesDTOList(courseDAO.findByName(name, page));
        if(courses.isEmpty()){
            throw new NullPointerException();
        }
        else{
            return courses;
        }
    }

    public List<CourseDTO> findCourseByDepartment(int id, int page) {
        List<CourseDTO> courses = coursesDTOList(courseDAO.findByDepartment(id, page));
        if(courses.isEmpty()){
            throw new NullPointerException();
        }
        else{
            return courses;
        }
    }

    public List<CourseDTO> findCourseByTheme(int id, int page) {
        List<CourseDTO> courses = coursesDTOList(courseDAO.findByTheme(id, page));
        if(courses.isEmpty()){
            throw new NullPointerException();
        }
        else{
            return courses;
        }
    }

    public RevenueDTO findSoldByThemes(int id) {
        RevenueDTO revenue = new RevenueDTO(courseDAO.findSoldByThemes(id), themeModelToDTO(themeOfCourseDAO.findById(id)));
        if(revenue == null){
            throw new NullPointerException();
        }
        else{
            return revenue;
        }
    }

    public DepartmentDTO departmentModelToDTO(Department department){
        DepartmentDTO dto = new DepartmentDTO(department.getId(), department.getName());
        return dto;
    }

    public ThemeOfCourseDTO themeModelToDTO(ThemeOfCourse themeOfCourse){
        ThemeOfCourseDTO dto = new ThemeOfCourseDTO(themeOfCourse.getId(), themeOfCourse.getScience());
        return dto;
    }

    public CourseDTO courseModelToDTO(Course course){
        CourseDTO dto = new CourseDTO(course.getId(), course.getName(), themeModelToDTO(course.getThemeOfCourse()), course.getPrice(), course.getSoldCount(), departmentModelToDTO(course.getDepartment()));
        return dto;
    }

    public List<DepartmentDTO> departmentDTOList(List<Department> departments){
        List<DepartmentDTO> departmentDTOList = new ArrayList<>();
        DepartmentDTO departmentDTO;
        for (int i=0; i<departments.size(); i++){
            departmentDTO = new DepartmentDTO(departments.get(i).getId(), departments.get(i).getName());
            departmentDTOList.add(departmentDTO);
        }
        return departmentDTOList;
    }

    public List<ThemeOfCourseDTO> themeDTOList(List<ThemeOfCourse> theme){
        List<ThemeOfCourseDTO> themeOfCourseDTOList = new ArrayList<>();
        ThemeOfCourseDTO themeOfCourseDTO;
        for (int i=0; i<theme.size(); i++){
            themeOfCourseDTO = new ThemeOfCourseDTO(theme.get(i).getId(), theme.get(i).getScience());
            themeOfCourseDTOList.add(themeOfCourseDTO);
        }
        return themeOfCourseDTOList;
    }

    public List<CourseDTO> coursesDTOList(List<Course> courses){
        List<CourseDTO> courseDTOList = new ArrayList<>();
        CourseDTO courseDTO;
        for (int i=0; i<courses.size(); i++){
            courseDTO =
                    new CourseDTO(courses.get(i).getId(), courses.get(i).getName(), themeModelToDTO(courses.get(i).getThemeOfCourse()),
                            courses.get(i).getPrice(), courses.get(i).getSoldCount(), departmentModelToDTO(courses.get(i).getDepartment()));
            courseDTOList.add(courseDTO);
        }
        return courseDTOList;
    }
}
