package models;

import javax.persistence.*;


@Entity
@Table(name = "courses")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "theme_id")
    private ThemeOfCourse themeOfCourse;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "department_id")
    private Department department;

    @Column(name = "price")
    private int price;

    @Column(name = "sold_count")
    private int soldCount;

    public Course(int id, String name, Integer price, Integer soldCount){
        this.id = id;
        this.name = name;
        this.price = price;
        this.soldCount = soldCount;
    }

    public Course() {

    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) { this.id = id; }

    public  String getName(){
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice(){
        return this.price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Integer getSoldCount(){
        return this.soldCount;
    }

    public void setSoldCount(int soldCount) {
        this.soldCount = soldCount;
    }

    public ThemeOfCourse getThemeOfCourse() { return this.themeOfCourse;}

    public void setThemeOfCourse(ThemeOfCourse themeOfCourse) {
        this.themeOfCourse = themeOfCourse;
    }

    public Department getDepartment() {
        return this.department;
    }

    public void setDepartment(Department department){
        this.department = department;
    }

    @Override
    public String toString() {
        return "models.Course{" +
                "name=" + name + ", " +
                "price=" + price + ", " +
                "soldCount=" + soldCount + ", " +
                "theme_id=" + getThemeOfCourse().toString() + ", " +
                "department_id=" + getDepartment().toString() + "}";
    }


}
