package models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "themes_of_courses")
public class ThemeOfCourse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "science")
    private String science;

    @OneToMany(mappedBy = "themeOfCourse", cascade = CascadeType.ALL)
    private List<Course> courses;

    public ThemeOfCourse() {
    }

    public ThemeOfCourse(String science) {
        this.science = science;
        this.courses = new ArrayList<>();
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) { this.id = id; }

    public String getScience() {
        return this.science;
    }

    public void setScience(String science) {
        this.science = science;
    }

    public List<Course> getCourses() {
        return this.courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    @Override
    public String toString() {
        return "models.Theme_Of_Course{" +
                "id=" + id +
                ", science='" + science +
                '}';
    }
}
