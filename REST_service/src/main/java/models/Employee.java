package models;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "employees")
public class Employee {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "special_number")
    private UUID id;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "age")
    private int age;

    @Column(name = "employment_date")
    private Date employmentDate;

    @Column(name = "passport_data",unique=true)
    private String passportData;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "department_id")
    private Department department;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "position_id")
    private Position position;

    public Employee(){
    }

    public Employee(String lastName, String firstName, int age, Date employmentDate, String passportData){
        this.lastName = lastName;
        this.firstName = firstName;
        this.age = age;
        this.employmentDate = employmentDate;
        this.passportData = passportData;
    }

    public UUID getId() { return this.id; }

    public void setId(UUID id) {
        this.id = id;
    }

    public  String getLastName(){
        return this.lastName;
    }

    public void setLastName(String last_name) { this.lastName = last_name; }

    public  String getFirstName(){
        return this.firstName;
    }

    public void setFirstName(String first_name) { this.firstName = first_name; }

    public Integer getAge(){
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getEmploymentDate(){
        return this.employmentDate;
    }

    public void setEmploymentDate(Date employment_date) {
        this.employmentDate = employment_date;
    }

    public  String getPassportData(){
        return this.passportData;
    }

    public void setPassportData(String passport_data) { this.passportData = passport_data; }

    public Position getPosition() { return this.position; }

    public void setPosition(Position position){
        this.position = position;
    }

    public Department getDepartment() {
        return this.department;
    }

    public void setDepartment(Department department){
        this.department = department;
    }

    @Override
    public String toString() {
        return "models.Employee{" +
                "special_number=" + id + ", " +
                "last_name=" + lastName + ", " +
                "first_name=" + firstName + ", " +
                "age=" + age + ", " +
                "employment_date=" + employmentDate + ", " +
                "passport_data=" + passportData + ", " +
                "department=" + getDepartment().toString() + ", " +
                "position=" + getPosition().toString() + "}";
    }
}
