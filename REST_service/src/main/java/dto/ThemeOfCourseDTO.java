package dto;

public class ThemeOfCourseDTO {
    private int id;
    private String science;

    public ThemeOfCourseDTO(int id, String science) {
        this.id = id;
        this.science = science;
    }

    public ThemeOfCourseDTO() {
    }

    public int getId() { return id; }

    public String getScience() { return science; }

    public void setScience(String science) { this.science = science; }
}
