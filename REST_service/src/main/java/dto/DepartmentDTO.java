package dto;

public class DepartmentDTO {

   private int id;
   private String name;

    public DepartmentDTO(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public DepartmentDTO() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) { this.name = name; }
}
