package dto;

public class PositionDTO {

    private int id;
    private String name;
    private int salary;

    public PositionDTO(int id, String name, int salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    public PositionDTO() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
