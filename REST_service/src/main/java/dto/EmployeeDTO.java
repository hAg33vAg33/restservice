package dto;

import java.util.Date;
import java.util.UUID;

public class EmployeeDTO {
    private UUID id;
    private String lastName;
    private String firstName;
    private int age;
    private Date employmentDate;
    private String passportData;
    private DepartmentDTO department;
    private PositionDTO position;

    public EmployeeDTO(UUID id, String lastName, String firstName, int age, Date employmentDate, String passportData, DepartmentDTO department, PositionDTO position) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.age = age;
        this.employmentDate = employmentDate;
        this.passportData = passportData;
        this.department = department;
        this.position = position;
    }

    public EmployeeDTO() {
    }

    public UUID getId() { return this.id; }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getEmploymentDate() {
        return this.employmentDate;
    }

    public void setEmploymentDate(Date employmentDate) {
        this.employmentDate = employmentDate;
    }

    public String getPassportData() {
        return this.passportData;
    }

    public void setPassportData(String passportData) {
        this.passportData = passportData;
    }

    public DepartmentDTO getDepartment() {
        return this.department;
    }

    public void setDepartment(DepartmentDTO department) {
        this.department = department;
    }

    public PositionDTO getPosition() {
        return this.position;
    }

    public void setPosition(PositionDTO position) {
        this.position = position;
    }
}
