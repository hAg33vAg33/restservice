package dto;

public class CourseDTO {
    private int id;
    private String name;
    private ThemeOfCourseDTO themeOfCourse;
    private int price;
    private int soldCount;
    private DepartmentDTO department;

    public CourseDTO(int id, String name, ThemeOfCourseDTO themeOfCourse, int price, int soldCount, DepartmentDTO department) {
        this.id = id;
        this.name = name;
        this.themeOfCourse = themeOfCourse;
        this.price = price;
        this.soldCount = soldCount;
        this.department = department;
    }

    public CourseDTO() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ThemeOfCourseDTO getThemeOfCourse() { return this.themeOfCourse; }

    public void setThemeOfCourse(ThemeOfCourseDTO themeOfCourse) {
        this.themeOfCourse = themeOfCourse;
    }

    public DepartmentDTO getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentDTO department) {
        this.department = department;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getSoldCount() {
        return soldCount;
    }

    public void setSoldCount(int soldCount) {
        this.soldCount = soldCount;
    }
}
