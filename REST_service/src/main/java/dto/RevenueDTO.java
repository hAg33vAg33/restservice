package dto;

public class RevenueDTO {
    private Long revenue;

    private ThemeOfCourseDTO theme;

    public RevenueDTO(Long revenue, ThemeOfCourseDTO theme) {
        this.revenue = revenue;
        this.theme = theme;
    }

    public ThemeOfCourseDTO getTheme() { return this.theme; }

    public void setTheme(ThemeOfCourseDTO theme) {
        this.theme = theme;
    }

    public RevenueDTO() {
    }

    public Long getRevenue() {
        return revenue;
    }

    public void setRevenue(Long revenue) {
        this.revenue = revenue;
    }
}
