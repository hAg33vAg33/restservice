package controllers;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import models.Course;
import services.CourseService;

@Path("/general")
public class CourseController {

    CourseService courseService = new CourseService();

    @GET
    @Path("/courses/view/{page}")
    @Produces("application/json")
    public Response getCourses(@PathParam("page") int page) {
        try {
            return Response.status(200).entity(courseService.findAll(page)).build();
        }catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }

    }

    @GET
    @Path("/courses/view/id/{id}")
    @Produces("application/json")
    public Response getCoursesById(@PathParam("id") int id) {
        try {
            return Response.status(200).entity(courseService.findCourseById(id)).build();
        }catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/courses/view/name/{name}/{page}")
    @Produces("application/json")
    public Response getCoursesByName(@PathParam("name") String name, @PathParam("page") int page) {
        try {
            return Response.status(200).entity(courseService.findCourseByName(name, page)).build();
        }catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/courses/view/department/{id}/{page}")
    @Produces("application/json")
    public Response getCoursesDepartment(@PathParam("id") int id, @PathParam("page") int page) {
        try {
            return Response.status(200).entity(courseService.findCourseByDepartment(id, page)).build();
        }catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/courses/view/theme/{id}/{page}")
    @Produces("application/json")
    public Response getCoursesByTheme(@PathParam("id") int id, @PathParam("page") int page) {
        try {
            return Response.status(200).entity(courseService.findCourseByTheme(id, page)).build();
        }catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/courses/view/revenue/{id}")
    @Produces("application/json")
    public Response getSoldByThemes(@PathParam("id") int id) {
        try {
            return Response.status(200).entity(courseService.findSoldByThemes(id)).build();
        }catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @POST
    @Path("/courses/add")
    @Consumes("application/json")
    @Produces("application/json")
    public Response setCourse(Course course){
        try {
            courseService.saveCourse(course);
            return Response.status(201).entity("New course" + course.getName() + " " + course.getThemeOfCourse() + " " +
                    " is just has been added ").build();
        }
        catch(Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @DELETE
    @Path("/courses/delete/{id}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response deleteCourse(@PathParam("id") int id){
        try {
            courseService.deleteCourse(id);
            return Response.status(201).entity("You just deleted that course! Nice work!").build();
        }
        catch(Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @PUT
    @Path("/courses/update")
    @Consumes("application/json")
    @Produces("application/json")
    public Response updateCourse(Course course){
        try {
            courseService.updateCourse(course);
            return Response.status(201).entity("You just updated that course! Nice work!").build();
        }
        catch(Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }


}
