package controllers;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import models.Department;
import services.DepartmentService;




@Path("/general")
public class DepartmentController {

    DepartmentService departmentService = new DepartmentService();

    @GET
    @Path("/departments/view/{page}")
    @Produces("application/json")
    public Response getDepartments(@PathParam("page") int page) {
        try {
            return Response.status(200).entity(departmentService.findAllDepartments(page)).build();
        }catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }


    @GET
    @Path("/departments/view/name/{name}/{page}")
    @Produces("application/json")
    public Response getDepartmentsByName(@PathParam("name") String name, @PathParam("page") int page) {
        try {
            return Response.status(200).entity(departmentService.findDepartmentByName(name, page)).build();
        }
        catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/departments/view/id/{id}")
    @Produces("application/json")
    public Response getDepartmentsById(@PathParam("id") int id) {
        try {
            return Response.status(200).entity(departmentService.findDepartmentById(id)).build();
        }
        catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }

    }

    @POST
    @Path("/departments/add")
    @Consumes("application/json")
    @Produces("application/json")
    public Response setDepartment(Department department){
       try {
           departmentService.saveDepartment(department);
           return Response.status(201).entity("New department in " + department.getName() +
                   " is just has been added ").build();
       }
       catch(Exception e){
           return Response.status(400).entity(e.getMessage()).build();
       }
    }

    @DELETE
    @Path("/departments/delete/{id}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response deleteDepartment(@PathParam("id") int id){
        try {
            departmentService.deleteDepartment(id);
            return Response.status(201).entity("You just deleted that department! Nice work!").build();
        }
        catch(Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @PUT
    @Path("/departments/update")
    @Consumes("application/json")
    @Produces("application/json")
    public Response updateDepartment(Department department){
        try {
            departmentService.updateDepartment(department);
            return Response.status(201).entity("You just updated that department! Nice work!").build();
        }
        catch(Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }


}
