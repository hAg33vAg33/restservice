package controllers;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import models.ThemeOfCourse;
import services.ThemeOfCourseService;

@Path("/general")
public class ThemeOfCourseController {

    ThemeOfCourseService themeOfCourseService = new ThemeOfCourseService();

    @GET
    @Path("/themes/view/{page}")
    @Produces("application/json")
    public Response getThemes(@PathParam("page") int page) {
        try {
            return Response.status(200).entity(themeOfCourseService.findAllThemes(page)).build();
        }catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/themes/view/id/{id}")
    @Produces("application/json")
    public Response getThemesById(@PathParam("id") int id) {
        try {
            return Response.status(200).entity(themeOfCourseService.findThemesById(id)).build();
        }catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/themes/view/name/{name}/{page}")
    @Produces("application/json")
    public Response getThemesByName(@PathParam("name") String name, @PathParam("page") int page) {
        try {
            return Response.status(200).entity(themeOfCourseService.findThemesByName(name, page)).build();
        }catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @POST
    @Path("/themes/add")
    @Consumes("application/json")
    @Produces("application/json")
    public Response setTheme(ThemeOfCourse theme){
        try {
            themeOfCourseService.saveTheme(theme);
            return Response.status(201).entity("New theme from " + theme.getScience() +
                    " is just has been added ").build();
        }
        catch(Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @DELETE
    @Path("/themes/delete/{id}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response deleteTheme(@PathParam("id") int id){
        try {
            themeOfCourseService.deleteTheme(id);
            return Response.status(201).entity("You just deleted that theme! Nice work!").build();
        }
        catch(Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @PUT
    @Path("/themes/update")
    @Consumes("application/json")
    @Produces("application/json")
    public Response updateTheme(ThemeOfCourse theme){
        try {
            themeOfCourseService.updateTheme(theme);
            return Response.status(201).entity("You just updated that theme! Nice work!").build();
        }
        catch(Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }


}
