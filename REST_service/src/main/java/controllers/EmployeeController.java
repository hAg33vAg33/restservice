package controllers;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import models.Employee;
import services.EmployeeService;

import java.util.UUID;

@Path("/general")
public class EmployeeController {

    EmployeeService employeeService = new EmployeeService();


    @GET
    @Path("/employees/view/{page}")
    @Produces("application/json")
    public Response getEmployees(@PathParam("page") int page) {
        try {
            return Response.status(200).entity(employeeService.findAllEmployees(page)).build();
        }catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/employees/view/departmentandposition/{departmentid}/{positionid}/{page}")
    @Produces("application/json")
    public Response getEmployeesByDepartmentAndPosition(@PathParam("departmentid") int departmentId,
                                         @PathParam("positionid") int positionId, @PathParam("page") int page) {
        try {
            return Response.status(200).entity(employeeService.findByDepartmentAndPosition(departmentId, positionId, page)).build();
        }
        catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/employees/view/department/{departmentid}/{page}")
    @Produces("application/json")
    public Response getEmployeesByDepartment(@PathParam("departmentid") int departmentId, @PathParam("page") int page) {
        try {
            return Response.status(200).entity(employeeService.findByDepartment(departmentId, page)).build();
        }
        catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/employees/view/position/{positionid}/{page}")
    @Produces("application/json")
    public Response getEmployeesByPosition(@PathParam("positionid") int positionId, @PathParam("page") int page) {
        try {
            return Response.status(200).entity(employeeService.findByPosition(positionId, page)).build();
        }
        catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/employees/view/daterange/{startdate}/{enddate}/{page}")
    @Produces("application/json")
    public Response getEmployeesByEmploymentDateRange(@PathParam("startdate") String startDate,
                                           @PathParam("enddate") String endDate, @PathParam("page") int page) {
        try {
            return Response.status(200).entity(employeeService.findByEmploymentDateRange(startDate, endDate, page)).build();
        }
        catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/employees/view/date/{date}/{page}")
    @Produces("application/json")
    public Response getEmployeesByEmploymentDate(@PathParam("date") String date, @PathParam("page") int page) {
        try {
            return Response.status(200).entity(employeeService.findByEmploymentDate(date, page)).build();
        }
        catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/employees/view/firstname/{firstname}/{page}")
    @Produces("application/json")
    public Response getEmployeesByFirstName(@PathParam("firstname") String firstName, @PathParam("page") int page) {
        try {
            return Response.status(200).entity(employeeService.findByFirstName(firstName, page)).build();
        }
        catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/employees/view/lastname/{lastname}/{page}")
    @Produces("application/json")
    public Response getEmployeesByLastName(@PathParam("lastname") String lastName, @PathParam("page") int page) {
        try {
            return Response.status(200).entity(employeeService.findByLastName(lastName, page)).build();
        }
        catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/employees/view/passportdata/{passportdata}/{page}")
    @Produces("application/json")
    public Response getEmployeesByPassportData(@PathParam("passportdata") String passportData, @PathParam("page") int page) {
        try {
            return Response.status(200).entity(employeeService.findByPassportData(passportData, page)).build();
        }
        catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/employees/view/age/{age}/{page}")
    @Produces("application/json")
    public Response getEmployeesByAge(@PathParam("age") int age, @PathParam("page") int page) {
        try {
            return Response.status(200).entity(employeeService.findByAge(age, page)).build();
        }
        catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/employees/view/agerange/{startage}/{endage}/{page}")
    @Produces("application/json")
    public Response getEmployeesByAgeRange(@PathParam("startage") int startAge,
                                           @PathParam("endage") int endAge,
                                           @PathParam("page") int page) {
        try {
            return Response.status(200).entity(employeeService.findByAgeRange(startAge, endAge, page)).build();
        }
        catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/employees/view/agesorted/{page}")
    @Produces("application/json")
    public Response getEmployeesByAgeSorted(@PathParam("page") int page) {
        try {
            return Response.status(200).entity(employeeService.findByAgeSorted(page)).build();
        }
        catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }


    @GET
    @Path("/employees/view/datesorted/{page}")
    @Produces("application/json")
    public Response getEmployeesByEmploymentDateSorted(@PathParam("page") int page) {
        try {
            return Response.status(200).entity(employeeService.findByEmploymentDateSorted(page)).build();
        }
        catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/employees/view/agemax")
    @Produces("application/json")
    public Response getEmployeesAgeMax() {
        try {
            return Response.status(200).entity(employeeService.findByAgeMax()).build();
        }
        catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/employees/view/extended/{search}/{page}")
    @Produces("application/json")
    public Response extendedSearch(@PathParam("search") String search, @PathParam("page") int page) {
        try {
            return Response.status(200).entity(employeeService.extendedSearch(search, page)).build();
        }
        catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @POST
    @Path("/employees/add")
    @Consumes("application/json")
    @Produces("application/json")
    public Response setEmployee(Employee employee){
        try {
            employeeService.saveEmployee(employee);
            return Response.status(201).entity("New employee" + employee.getFirstName() + " " + employee.getLastName() + " "+
                    " is just has been added ").build();
        }
        catch(Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @DELETE
    @Path("/employees/delete/{specialnumber}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response deleteEmployee(@PathParam("specialnumber") UUID specialNumber){
        try {
            employeeService.deleteEmployee(specialNumber);
            return Response.status(201).entity("You just deleted that employee! Nice work!").build();
        }
        catch(Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @PUT
    @Path("/employees/update")
    @Consumes("application/json")
    @Produces("application/json")
    public Response updateEmployee(Employee employee){
        try {
            //employeeService.updateEmployee(specialNumber, departmentId, positionId, lastName, firstName, age, employmentDate, passportData);
            employeeService.updateEmployee(employee);
            return Response.status(201).entity("You just updated that employee! Nice work!").build();
        }
        catch(Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }



}
