package controllers;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import models.Position;
import services.PositionService;

@Path("/general")
public class PositionController {
    PositionService positionService = new PositionService();

    @GET
    @Path("/positions/view/{page}")
    @Produces("application/json")
    public Response getPositions(@PathParam("page") int page) {
        try {
            return Response.status(200).entity(positionService.findAllPositions(page)).build();
        }catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/positions/view/name/{name}/{page}")
    @Produces("application/json")
    public Response getPositionsByName(@PathParam("name") String name, @PathParam("page") int page) {
        try {
            return Response.status(200).entity(positionService.findPositionsByName(name, page)).build();
        }
        catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/positions/view/id/{id}")
    @Produces("application/json")
    public Response getPositionsById(@PathParam("id") int id) {
        try {
            return Response.status(200).entity(positionService.findPositionById(id)).build();
        }
        catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }

    }

    @GET
    @Path("/positions/view/salarysorted/{page}")
    @Produces("application/json")
    public Response getPositionsSalarySorted(@PathParam("page") int page) {
        try {
            return Response.status(200).entity(positionService.findBySalarySorted(page)).build();
        }
        catch (Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @POST
    @Path("/positions/add")
    @Consumes("application/json")
    @Produces("application/json")
    public Response setPosition(Position position){
        try {
            positionService.savePosition(position);
            return Response.status(201).entity("New position " + position.getName() +
                    " is just has been added ").build();
        }
        catch(Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @DELETE
    @Path("/positions/delete/{id}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response deletePosition(@PathParam("id") int id){
        try {
            positionService.deletePosition(id);
            return Response.status(201).entity("You just deleted that position! Nice work!").build();
        }
        catch(Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @PUT
    @Path("/positions/update")
    @Consumes("application/json")
    @Produces("application/json")
    public Response updatePosition(Position position){
        try {
            positionService.updatePosition(position);
            return Response.status(201).entity("You just updated that position! Nice work!").build();
        }
        catch(Exception e){
            return Response.status(400).entity(e.getMessage()).build();
        }
    }
}
