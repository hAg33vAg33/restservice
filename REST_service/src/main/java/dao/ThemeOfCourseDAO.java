package dao;


import models.ThemeOfCourse;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateSessionFactoryUtil;

import java.util.List;

public class ThemeOfCourseDAO {


    public List<ThemeOfCourse> findAll(int page){
        String hql = "from ThemeOfCourse";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public ThemeOfCourse findById(int id){
        String hql = "from ThemeOfCourse where id = :themeId";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("themeId", id);
        return (ThemeOfCourse) query.getSingleResult();
    }

    public List<ThemeOfCourse> findByScience(String name, int page){
        String hql = "from ThemeOfCourse where lower(science) like :science";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("science", "%"+name.toLowerCase()+"%");
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public void save(ThemeOfCourse themeOfCourse) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(themeOfCourse);
        tx1.commit();
        session.close();
    }

    public void update(ThemeOfCourse themeOfCourse) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(themeOfCourse);
        tx1.commit();
        session.close();
    }

    public void delete(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        String hql = "delete ThemeOfCourse where id = :themeOfCourseId";
        Query query = session.createQuery(hql);
        query.setParameter("themeOfCourseId", id);
        query.executeUpdate();
        tx1.commit();
        session.close();
    }
}
