package dao;

import models.Employee;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateSessionFactoryUtil;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class EmployeeDAO {

    public List<Employee> findAll(int page){
        String hql = "from Employee";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public Employee findBySpecialNumber(UUID id){
        String hql = "from Employee where id = :employeeId";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("employeeId", id);
        return (Employee) query.getSingleResult();
    }

    public List<Employee> findByDepartmentAndPosition(int departmentId, int positionId, int page){
        String hql = "from Employee where department.id = :departmentId and position.id = :positionId";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("departmentId", departmentId);
        query.setParameter("positionId", positionId);
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public List<Employee> findByDepartment(int id, int page){
        String hql = "from Employee  where department.id = :departmentId";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("departmentId", id);
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public List<Employee> findByPosition(int id, int page){
        String hql = "from Employee where position.id = :positionId";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("positionId", id);
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public List<Employee> findByEmploymentDateRange(Date startDate, Date endDate, int page){
        String hql = "from Employee where employmentDate >= :startDate and employmentDate <= :endDate";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public List<Employee> findByEmploymentDate(Date date, int page){
        String hql = "from Employee where employmentDate = :employmentDate";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("employmentDate", date);
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public List<Employee> findByFirstName(String firstName, int page){
        String hql = "from Employee where lower(firstName) like :firstName";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("firstName", "%"+firstName.toLowerCase()+"%");
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public List<Employee> findByLastName(String lastName, int page){
        String hql = "from Employee where lower(lastName) like :lastName";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("lastName", "%"+lastName.toLowerCase()+"%");
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public List<Employee> findByPassportData(String passportData, int page){
        String hql = "from Employee where passportData like :passportData";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("passportData", "%"+passportData+"%");
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public List<Employee> findByAge(int age, int page){
        String hql = "from Employee where age = :age";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("age", age);
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public List<Employee> findByAgeRange(int startAge, int endAge, int page){
        String hql = "from Employee where age >= :startAge and age <= :endAge";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("startAge", startAge);
        query.setParameter("endAge", endAge);
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public List<Employee> findByAgeSorted(int page){
        String hql = "from Employee order by age asc ";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public List<Employee> findByEmploymentDateSorted(int page){
        String hql = "from Employee order by employmentDate asc";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public Employee findByAgeMax(){
        String hql = "FROM Employee WHERE age = (SELECT MAX(age) FROM Employee)";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        return (Employee) query.getSingleResult();
    }

    public List<Employee> extendedSearch(String search, int page){
        String hql = "FROM Employee WHERE lower(firstName) like :search or lower(lastName) like :search or lower(passportData) like :search";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("search", "%"+search.toLowerCase()+"%");
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }


    public void save(Employee employee) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(employee);
        tx1.commit();
        session.close();
    }

    public void update(Employee employee) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(employee);
        tx1.commit();
        session.close();
    }

    public void delete(Employee employee) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(employee);
        tx1.commit();
        session.close();
    }

}
