package dao;

import models.Department;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateSessionFactoryUtil;
import java.util.List;


public class DepartmentDAO {

    public List<Department> findAll(int page){
        String hql = "from Department";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public Department findById(int id){
        String hql = "from Department where id = :departmentId";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("departmentId", id);
        return (Department) query.getSingleResult();
    }

    public List<Department> findByName(String name, int page){
        String hql = "from Department where lower(name) like :departmentName";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("departmentName", "%"+name.toLowerCase()+"%");
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public void save(Department department) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(department);
        tx1.commit();
        session.close();
    }

    public void update(Department department) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(department);
        tx1.commit();
        session.close();
    }

    public void delete(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        String hql = "delete Department where id = :departmentId";
        Query query = session.createQuery(hql);
        query.setParameter("departmentId", id);
        query.executeUpdate();
        tx1.commit();
        session.close();
    }

}
