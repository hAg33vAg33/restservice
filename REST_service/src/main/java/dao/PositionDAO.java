package dao;

import models.Position;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateSessionFactoryUtil;

import java.util.List;

public class PositionDAO {

    public List<Position> findAll(int page){
        String hql = "from Position";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public Position findById(int id){
        String hql = "from Position where id = :positionId";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("positionId", id);
        return (Position) query.getSingleResult();
    }

    public List<Position> findByName(String name, int page){
        String hql = "from Position where lower(name) like :positionName";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("positionName", "%"+name.toLowerCase()+"%");
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public List<Position> findBySalarySorted(int page){
        String hql = " from Position order by salary asc";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public void save(Position position) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(position);
        tx1.commit();
        session.close();
    }

    public void update(Position position) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(position);
        tx1.commit();
        session.close();
    }

    public void delete(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        String hql = "delete Position where id = :positionId";
        Query query = session.createQuery(hql);
        query.setParameter("positionId", id);
        query.executeUpdate();
        tx1.commit();
        session.close();
    }
}
