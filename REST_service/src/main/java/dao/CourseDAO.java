package dao;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import models.Course;

import utils.HibernateSessionFactoryUtil;

import java.util.List;


public class CourseDAO {

    public List<Course> findAll(int page){
        String hql = "from Course";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public Course findById(int id){
        String hql = "from Course where id = :courseId";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("courseId", id);
        return (Course) query.getSingleResult();
    }

    public List<Course> findByName(String name, int page){
        String hql = "from Course where lower(name) like :courseName";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("courseName", "%"+name.toLowerCase()+"%");
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public List<Course> findByDepartment(int id, int page){
        String hql = "from Course where department.id = :departmentId";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("departmentId", id);
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public List<Course> findByTheme(int id, int page){
        String hql = "from Course where themeOfCourse.id = :themeId";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("themeId", id);
        if (page!=0){page*=2;}
        query.setFirstResult(page);
        query.setMaxResults(2);
        return query.list();
    }

    public long findSoldByThemes(int id){
        String hql = "select sum(soldCount) * sum(price) as Revenue from Course where themeOfCourse.id = :themeOfCourseId";
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(hql);
        query.setParameter("themeOfCourseId", id);
        long result= (long)query.getSingleResult();
        return result;
    }

    public void save(Course course) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(course);
        tx1.commit();
        session.close();
    }

    public void update(Course course) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(course);
        tx1.commit();
        session.close();
    }

    public void delete(Course course) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(course);
        tx1.commit();
        session.close();
    }




}
